﻿using System;
using System.Collections.Generic;
using System.IO; //
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BookedAppLogic;
using Windows.Globalization.DateTimeFormatting;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserSelection
    {
        //initiating the field variables of the page
        private List<Client> _clients;
        private List<Doctor> _doctors;
        private Client _client;
        public UserSelection()
        {
            this.InitializeComponent();

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //creating a tuple to handle the passing of the variables from the mainpage
            ValueTuple<List<Client>, List<Doctor>, Client> tuple = (ValueTuple<List<Client>, List<Doctor>,Client>)e.Parameter;
            _client = tuple.Item3;
            _doctors = tuple.Item2;
            _clients = tuple.Item1;
            base.OnNavigatedTo(e);
        }
        //navigates the user to the sign in page
        private void _OnSignInClick(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SignInForm), (_clients,_doctors));
        }
        //navigates the user to the sign up page
        private void _OnSignupClick(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ClientSignUp), (_clients,_doctors));
        }
        
    }
}