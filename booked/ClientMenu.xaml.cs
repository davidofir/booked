﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BookedAppLogic;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ClientMenu : Page
    {
        private Client _client;
        private List<Doctor> _doctors;
        public ClientMenu()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ValueTuple<List<Doctor>,Client> tuple = (ValueTuple<List<Doctor>,Client>)e.Parameter;
            _doctors = tuple.Item1;
            _client = tuple.Item2;
            _txtWelcome.Text +=$" {_client.Name}.";
            base.OnNavigatedTo(e);
        }

        private void _OnClickBook(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(BookingPage),(_client,_doctors));
        }

        private void _OnClickView(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ViewAppointmentPage),(_client,_doctors));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(DoctorSignIn),_doctors);
        }

        private void _OnClickCancel(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(CancelAppointmentPage),(_doctors,_client));
        }
    }
}
