﻿using BookedAppLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewAppointmentsDoc : Page
    {
        private Doctor _doctor;

        public ViewAppointmentsDoc()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Grab the list of appointments from the Doctor class

            _doctor = ((Doctor)e.Parameter);
            base.OnNavigatedTo(e);
            FindAppointments();
        }

        public void FindAppointments()
        {
            //iterate through the list of doctors appointments

            foreach (Appointment appointment in _doctor.UnavailAppointments)
            {
                _lstAppointmentList.Items.Add(appointment);
            }
        }
    }
}

