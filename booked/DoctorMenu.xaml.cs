﻿using BookedAppLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DoctorMenu : Page
    {
        private Doctor _doctor;
        public DoctorMenu()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _doctor = (Doctor)e.Parameter;
            _txtWelcome.Text += _doctor.Name;

            base.OnNavigatedTo(e);
        }
        //navigates to the manage appointments page and passes the doctor to it
        private void _ClickedManageAppointments(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ManageAppointment),_doctor);
        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }
        //navigates to the view appointments page of the doctor and passes the doctor
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ViewAppointmentsDoc), _doctor);
        }
    }
}
