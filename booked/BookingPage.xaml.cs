﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BookedAppLogic;
using System.Reflection;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Windows.UI.Popups;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BookingPage : Page
    {
        private Client _client;
        private List<Doctor> _doctors;
        private DateTime _selectedDate;
        private List<int> _selectedIndexes;
        public BookingPage()
        {
            this.InitializeComponent();
            _selectedIndexes = new List<int>();
        }
        //recieving the passed required variables from the ClientMenu page
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ValueTuple<Client, List<Doctor>> tuple = (ValueTuple<Client, List<Doctor>>)e.Parameter;
            _client = tuple.Item1;
            _doctors = tuple.Item2;
            for (int items = 0; items < _doctors.Count; items++)
            {
                cBox.Items.Add(_doctors[items].Name);
            }
            base.OnNavigatedTo(e);
        }
        private void _OnSelectedDate(CalendarView sender, CalendarViewSelectedDatesChangedEventArgs args)
        {
            try
            {
                //Checks if the selected combo box field is null
                if (cBox.SelectedValue != null)
                {
                    _selectedIndexes = new List<int>();//stores the elements of the day that was selected
                    availAppointments.Items.Clear();//clears the list view (in case the user selects another date) 
                    availAppointments.Visibility = Visibility.Visible;//makes the list view visible in case the user has both a date selected and a doctor
                    _selectedDate = args.AddedDates[0].DateTime;//takes date that was pressed on from our calendar view
                    for(int app=0;app < _doctors[cBox.SelectedIndex].AvailAppointments.Count; app++) //iterates through the appointments of the day
                    {
                        if (_doctors[cBox.SelectedIndex].AvailAppointments[app].Date.Day == _selectedDate.Day) //adds to our list view only the fields that match their day with the selected day's date
                        {
                            _selectedIndexes.Add(app);
                            availAppointments.Items.Add(_doctors[cBox.SelectedIndex].AvailAppointments[app].ToString()); 
                        }
                    }
                }
            }
            catch(COMException e) //handles the exception if the user clicks on the same date twice in a row, which enables the user to cancel their date selection
            {

            }
        }

        private void _OnBackNavigation(object sender, RoutedEventArgs e) // allows the user to navigate back to the previous page
        {
            On_BackRequested();
        }
        private bool On_BackRequested()
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame.CanGoBack)
            {
                rootFrame.GoBack();
                return true;
            }
            return false;
        }
        private async void availAppointments_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //books the appointment based on the user's selection of the appointment
            _doctors[cBox.SelectedIndex].AvailAppointments[_selectedIndexes[availAppointments.SelectedIndex]].BookAppointment(_client);
            // creates a message dialog that notifies that the appointment was booked successfully
            MessageDialog bookedMessage = new MessageDialog($"Appointment booked successfully at {_doctors[cBox.SelectedIndex].AvailAppointments[_selectedIndexes[availAppointments.SelectedIndex]].DateFormat()} on {_doctors[cBox.SelectedIndex].AvailAppointments[_selectedIndexes[availAppointments.SelectedIndex]].TimeFormat()}");
            //displays the message dialog
            await bookedMessage.ShowAsync(); 
            // changes the status of the appointment (moves it from the list of available appointments to the list of unavailable appointments and keeps them organized by the date field)
            _doctors[cBox.SelectedIndex].ChangeStatus(_doctors[cBox.SelectedIndex].AvailAppointments, _doctors[cBox.SelectedIndex].UnavailAppointments, _selectedIndexes[availAppointments.SelectedIndex]);
            //navigates back to the ClientMenu page
            this.Frame.Navigate(typeof(ClientMenu),(_doctors,_client));
        }
    }
}
