﻿using BookedAppLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;



namespace Booked
{
    /// <summary>
    /// This page is for displaying the UI related capabilities when a patient is trying to cancel an appointment
    /// </summary>
    public sealed partial class CancelAppointmentPage : Page
    {
        private List<Doctor> _doctors;
        private Client _client;
        private List<int> _appIndexes;
        private List<int> _docIndexes;
        public CancelAppointmentPage()
        {
            this.InitializeComponent();
        }

        private void _OnBackNavigation(object sender, RoutedEventArgs e)
        {
            On_BackRequested();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ValueTuple<List<Doctor>, Client> tuple = (ValueTuple<List<Doctor>, Client>)e.Parameter;
            _doctors = tuple.Item1;
            _client = tuple.Item2;
            _appIndexes = new List<int>();
            _docIndexes = new List<int>();
            for(int doc = 0; doc < _doctors.Count; doc++)
            {
                for(int app = 0; app < _doctors[doc].UnavailAppointments.Count; app++)
                {
                    if (_doctors[doc].UnavailAppointments[app].User.Email == _client.Email)
                    {
                        string fullAppointment ="Dr."+_doctors[doc].Name+", "+ _doctors[doc].UnavailAppointments[app].ToString();
                        _AppointmentsListView.Items.Add(fullAppointment);
                        _appIndexes.Add(app);
                        _docIndexes.Add(doc);
                    }
                }
            }
            base.OnNavigatedTo(e);
        }


        private bool On_BackRequested()
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame.CanGoBack)
            {
                rootFrame.GoBack();
                return true;
            }
            return false;
        }

        private async void _AppointmentsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try{
                MessageDialog msg = new MessageDialog($"Your appointment on {_doctors[_docIndexes[_AppointmentsListView.SelectedIndex]].UnavailAppointments[_appIndexes[_AppointmentsListView.SelectedIndex]].ToString()} for Dr.{_doctors[_docIndexes[_AppointmentsListView.SelectedIndex]].Name} was cancelled successfully");
                await msg.ShowAsync();
                msg = null;
                _doctors[_docIndexes[_AppointmentsListView.SelectedIndex]].UnavailAppointments[_appIndexes[_AppointmentsListView.SelectedIndex]].CancelAppointment();
                _doctors[_docIndexes[_AppointmentsListView.SelectedIndex]].ChangeStatus(_doctors[_docIndexes[_AppointmentsListView.SelectedIndex]].UnavailAppointments, _doctors[_docIndexes[_AppointmentsListView.SelectedIndex]].AvailAppointments, _appIndexes[_AppointmentsListView.SelectedIndex]);
                _AppointmentsListView.Items.Remove(_AppointmentsListView.SelectedItem);
            }
            catch(ArgumentOutOfRangeException)
            {

            }
        }
    }
}
