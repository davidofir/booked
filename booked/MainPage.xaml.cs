﻿using System;
using System.Collections.Generic;
using System.IO; //
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BookedAppLogic;
using Windows.Globalization.DateTimeFormatting;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //create a feild variable for the client, list of clients, and the list of doctors
        private List<Client> _clients;
        private Client _client;
        private List<Doctor> _doctors;
        public MainPage()
        {
            this.InitializeComponent();
            //creates some sample clients for the app to work with
            _clients = new List<Client>();
            _doctors = new List<Doctor>();
            _client = new Client("sample", "sample123@gmail.com", "password", "other");
            _clients.Add(new Client("Andrew", "andrew123@gmail.com", "password", "male"));
            _clients.Add(new Client("Michael", "michael123@gmail.com", "password", "male"));
            _clients.Add(new Client("Ofir", "ofir123@gmail.com", "password", "male"));
            _clients.Add(new Client("Stephanie", "stephanie123@gmail.com", "password123", "female"));
            //creates some doctors to work with
            _doctors.Add(new Dermatologist("Robert", "robert123@gmail.com", "password", "male", new TimeSpan(0, 30, 0), new DateTime(2020, 4, 20, 9, 0, 0), new TimeSpan(8, 0, 0), DayOfWeek.Monday));
            _doctors.Add(new Dermatologist("Jenny", "jenny123@gmail.com", "password", "female", new TimeSpan(0, 20, 0), new DateTime(2020, 4, 20, 10, 0, 0), new TimeSpan(9, 0, 0), DayOfWeek.Thursday));
            _doctors.Add(new FamilyDoctor("Eric", "eric123@gmail.com", "password", "male", new TimeSpan(0, 10, 0), new DateTime(2020, 4, 20, 7, 0, 0), new TimeSpan(10, 0, 0), DayOfWeek.Thursday));
            _doctors.Add(new FamilyDoctor("Erica", "erica123@gmail.com", "password", "female", new TimeSpan(0, 10, 0), new DateTime(2020, 4, 20, 8, 0, 0), new TimeSpan(10, 0, 0), DayOfWeek.Saturday));
            _doctors.Add(new Dentist("Mark", "mark123@gmail.com", "password", "male", new TimeSpan(0, 45, 0), new DateTime(2020, 4, 20, 9, 0, 0), new TimeSpan(8, 0, 0), DayOfWeek.Sunday));
            _doctors.Add(new Dentist("Amanda", "amanda123@gmail.com", "password", "female", new TimeSpan(0, 45, 0), new DateTime(2020, 4, 20, 10, 0, 0), new TimeSpan(8, 0, 0), DayOfWeek.Saturday));
        }
        //allows the selection of either a client or a doctor and navigates to the respectful pages
        private void btnClient_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DoctorSignIn), _doctors);
        }

        private void btnClient_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(UserSelection), (_clients, _doctors, _client));
        }
    }
}
