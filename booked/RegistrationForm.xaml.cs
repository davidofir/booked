using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BookedAppLogic;

/// <summary>
/// Registration form is for patients to register their information and create a record for them. This record will be stored in a collection;dictionary, for manipulation.
/// The collection needed for a patient's infromation will be of a collection where patient information can be retrieved quickly since that is the most
/// used functionality reghardless of any scenario. Editing the content of the index is definetly an important and frequent action, but the speed of it is not too important when in use.
/// Therefore, a dictionary for a master patient list records seem to be reasonable for this situation.
/// </summary>

namespace Booked
{

    public sealed partial class RegistrationForm : Page
    {
        public RegistrationForm()
        {
            this.InitializeComponent();
        }
    }
}
