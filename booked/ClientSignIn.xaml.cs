﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BookedAppLogic;
using System.Data.SqlClient;
using System.Data;
using Windows.UI.Popups;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SignInForm : Page
    {
        private List<Client> _clients;
        private List<Doctor> _doctors;
        private Client _client;

        public SignInForm()
        {
            this.InitializeComponent();
        }

        //receiving the doctor and client list from the navigated page
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ValueTuple<List<Client>, List<Doctor>> tuple = (ValueTuple<List<Client>, List<Doctor>>)e.Parameter;
            _clients = tuple.Item1;
            _doctors = tuple.Item2;
            base.OnNavigatedTo(e);
        }

        //Event handler for when the user chooses to sign in 
        private async void Button_Click(object sender, RoutedEventArgs e)
        {

            string userEmail = emailBox.Text; //value which stores the users email
            string userPassword = passwordBox.Password; //value which stores the users password

            ValueTuple <bool, Client> validClient = VerifyUser(_clients, userEmail, userPassword); 
            bool valid = validClient.Item1;
            _client = validClient.Item2;
            
            if (valid == true)  //if the user is in the system then navigate to the next page
            {
                this.Frame.Navigate(typeof(ExploreServices),(_client,_doctors));
            }
            else //else display an error message
            {
                string message ="Username or password are incorrect. Please try again.";
                string title = "Error";
                MessageDialog msg = new MessageDialog(message, title);
                await msg.ShowAsync();
                return;
            }
        }

        //Method to compare the entered informaton with the information which is stroed in the system
        //iterate through each client in the client list and see if the entered information mathces both the email and  password
        public (bool,Client) VerifyUser(List<Client> clients, string email, string password)
        {
            for (int iClient = 0; iClient < clients.Count; iClient++)
            {
                if ((email == clients[iClient].Email) && (password == clients[iClient].Password))
                {
                    return (true, clients[iClient]);
                }
            }
            return (false, null);
        }
    }
}
