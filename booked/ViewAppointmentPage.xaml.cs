﻿using BookedAppLogic;   //Using for Appointments reference in ObservableCollection
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

/// <summary>
/// This page handles the interaction between the doctor or client using the xaml page to view their appointments
/// There is another page associated with the information of a specific appointment
/// </summary>

namespace Booked
{
    /// <summary>
    /// This page is for displaying appointments to user through the UI
    /// </summary>
    public sealed partial class ViewAppointmentPage : Page
    {
        private Client _client;
        private List<Doctor> _doctors;
        public ViewAppointmentPage()
        {
            this.InitializeComponent();

        }
        //takes the values that were passed from the ClientMenu page
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ValueTuple <Client,List<Doctor>> tuple=(ValueTuple<Client, List<Doctor>>)e.Parameter;
            _client = tuple.Item1;
            _doctors = tuple.Item2;
            foreach(Doctor doctor in _doctors)
            {
                foreach(Appointment app in doctor.UnavailAppointments)
                {
                    AppointmentsDisplay.Items.Add(app.ToString()+" for Dr."+doctor.Name);
                }
            }
        }
        //allows the navigation back of the app
        private void _OnBackNavigation(object sender, RoutedEventArgs e)
        {
            On_BackRequested();
        }
        private bool On_BackRequested()
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame.CanGoBack)
            {
                rootFrame.GoBack();
                return true;
            }
            return false;
        }
    }
}
