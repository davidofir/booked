﻿using BookedAppLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ClientSignUp : Page
    {

        private List<Client> _clients;
        private List<Doctor> _doctors;

        private string _gender;

        public ClientSignUp()
        {
            this.InitializeComponent();
        }

        //accept values from previous page
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ValueTuple<List<Client>, List<Doctor>> tuple = (ValueTuple<List<Client>, List<Doctor>>)e.Parameter;
            _clients = tuple.Item1;
            _doctors = tuple.Item2;
            base.OnNavigatedTo(e);
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            //check for errors in the form completion
            //Assign the values entered in the form to variables
            //Create a new client object with form variables.

            //check for missing first ot last name
            if (FName.Text == "" || LName.Text == "")
            {
                string message = "Missing first or last name.";
                string title = "Error";
                MessageDialog msg = new MessageDialog(message, title);
                msg.ShowAsync();
                return;
            }

            //check for empty email box
            if (EmailBox.Text == "")
            {
                string message = "Incorrect Email";
                string title = "Error";
                MessageDialog msg = new MessageDialog(message, title);
                msg.ShowAsync();
                return;
            }

            //Check to match sure that both password boxes are filled
            if (Password_txt.Password == "" || ConfirmPassword_txt.Password == "")
            {
                string message = "Please confirm password.";
                string title = "Error";
                MessageDialog msg = new MessageDialog(message, title);
                msg.ShowAsync();
                return;
            }

            //check to make sure a radial button is pressed
            if (MaleButton.IsChecked != true && FemaleButton.IsChecked != true && OtherButton.IsChecked != true)
            {
                string message = "Radiobutton was not checked.";
                string title = "Error";
                MessageDialog msg = new MessageDialog(message, title);
                msg.ShowAsync();
                return;
            }

            //check to make sure passwords match
            if (Password_txt.Password != ConfirmPassword_txt.Password)
            {
                string message = "Passwords do not match.";
                string title = "Error";
                MessageDialog msg = new MessageDialog(message, title);
                msg.ShowAsync();
                return;
            }

            string name = FName.Text + ' ' + LName.Text;
            string email = EmailBox.Text;
            string password = Password_txt.Password;
            if (MaleButton.IsChecked == true)
            {
                _gender = "Male";
            }
            if (FemaleButton.IsChecked == true)
            {
                _gender = "Female";
            }
            if (OtherButton.IsChecked == true)
            {
                _gender = "Other";
            }

            string gender = _gender;

            Client client = new Client(name, email, password, gender);

            _clients.Add(client);

            this.Frame.Navigate(typeof(SignInForm),(_clients,_doctors));

        }
    }
}
