﻿using BookedAppLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DoctorSignIn : Page
    {
        private List<Doctor> _doctors;
        private Doctor _doctor;

        public DoctorSignIn()
        {
            this.InitializeComponent();
        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _doctors = ((List<Doctor>)e.Parameter);
            base.OnNavigatedTo(e);
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            //take in the doctor ID and password
            //check if they match with the list of doctors
            //Navigate to the doctor menu if they do
            //display an error message if they do not

            int docID = (int)Convert.ToInt32(doctorID.Text);
            string userPassword = passwordBox.Password;

            ValueTuple<bool, Doctor> validDoctor = VerifyUser(_doctors, docID, userPassword);
            bool valid = validDoctor.Item1;
            _doctor = validDoctor.Item2;

            if (valid == true)
            {
                this.Frame.Navigate(typeof(DoctorMenu),_doctor);
            }
            else
            {
                string message = "Username or password are incorrect. Please try again.";
                string title = "Error";
                MessageDialog msg = new MessageDialog(message, title);
                msg.ShowAsync();
                return;
            }
        }

        public (bool,Doctor) VerifyUser(List<Doctor> doctors, int id, string password)
        {
            for (int iDoc = 0; iDoc < doctors.Count; iDoc++)
            {
                if ((id == doctors[iDoc].Id) && (password == doctors[iDoc].Password))
                {
                    return (true,doctors[iDoc]);
                }
            }
            return (false,null);
        }
    }
}
