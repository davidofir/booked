﻿using BookedAppLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Booked
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ExploreServices : Page
    {

        private List<Doctor> _dentistList;
        private List<Doctor> _familyDoctorList;
        private List<Doctor> _dermatologistList;
        private Client _client;
        private List<Doctor> _doctors;

        public ExploreServices()
        {
            this.InitializeComponent();
            _dentistList = new List<Doctor>();
            _familyDoctorList = new List<Doctor>();
            _dermatologistList = new List<Doctor>();
        }

        //accept values from previous page, client and doctor list
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ValueTuple<Client, List<Doctor>> tuple = (ValueTuple<Client, List<Doctor>>)e.Parameter;
            _client = tuple.Item1;
            _doctors = tuple.Item2;
            base.OnNavigatedTo(e);
        }

        private void On_Clicked(object sender, TappedRoutedEventArgs e)
        {
            byte iImage = (byte)Convert.ToInt32(((Image)sender).Tag); //get information about which image was clicked by the user
            
            if (iImage == 0) //If client chooses the dentist, load the correct list of dentists
            {
                foreach (Doctor doctor in _doctors)
                {
                    if (doctor is Dentist)
                    {
                        _dentistList.Add((Dentist)doctor);
                    }
                }
                Frame.Navigate(typeof(ClientMenu), (_dentistList, _client)); //pass doctor type and client to the client menu

            }
            if (iImage == 1) //If client chooses the family doctor, load the correct list of family doctor
            {
                foreach (Doctor doctor in _doctors)
                {
                    if (doctor is FamilyDoctor)
                    {
                        _familyDoctorList.Add((FamilyDoctor)doctor); 
                    }
                }
                Frame.Navigate(typeof(ClientMenu), (_familyDoctorList, _client)); //pass doctor type and client to the client menu
            }
            if (iImage == 2)  ////If client chooses the dermatologist, load the correct list of dermatologist 
            {
                foreach (Doctor doctor in _doctors)
                {
                    if (doctor is Dermatologist)
                    {
                        _dermatologistList.Add((Dermatologist)doctor);
                    }
                }
                Frame.Navigate(typeof(ClientMenu), (_dermatologistList, _client)); //pass doctor type and client to the client menu
            }
        }
    }
}
