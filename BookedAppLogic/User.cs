﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Description:
/// User class for a single user object. This class handles user data and itneracts with other classes where needed; appointments.
/// This class will read default hardcoded data from a text file acting as a "database" for the users of the application. Users will be 
/// added/removed from database. 
/// Technical:
/// This User class will be the base class for all user derived classes ; serviceProvider, of this application.
/// The User class consist of all related attributes and properties of a single User object.
/// </summary>

namespace BookedAppLogic
{
    public abstract class User
    {
        private string _name;
        private string _email;
        private string _password;
        private string _gender;

        public User(string name, string email, string password, string gender)
        {
            _name = name;
            _email = email;
            _password = password;
            _gender = gender;
        }

        //create propeties to access parameters of User
        public string Name
        {
           get { return _name; }
           set { _name = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
    }

}
