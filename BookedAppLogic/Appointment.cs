﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Credentials;

namespace BookedAppLogic
{
    public class Appointment
    {
        private DateTime _date;
        private Client _client;
        private TimeSpan _duration;
        public Appointment(DateTime date,TimeSpan duration)
        {
            _date=date;
            _duration = duration;
            _client = null;
        }
        #region Properties
        public DateTime Date { get => _date; set => _date = value; }
        public TimeSpan Duration { get => _duration; }
        //The end time is calculated by adding the time to the duration
        public DateTime EndTime { get => _date + _duration; }
        public User User {  get => _client; }
        #endregion Properties

        public bool isAvailable()
        {
            //our business logic dictates that as long as the user is null, the appointment is available.
            if (_client == null)
            {
                return true;
            }
            return false;
        }
        public void BookAppointment(Client client)
        {
            _client = client;
        }
        public string CancelAppointment()
        {
            _client = null;
            return $"Your appointment at {ToString().ToLower()} was cancelled";
        }
        public string DateFormat()
        {
            //formating our date for better a presentation
            return $"{_date.Month}/{_date.Day}/{_date.Year}";
        }
        //formatting our time for a better presentation
        public string TimeFormat()
        {
            return _date.ToString("hh:mm tt");
        }
        //creating a combination of the date and time
        public override string ToString()
        {
            return $"Date: {DateFormat()} and starts at: {TimeFormat()}";
        }

    }
}
