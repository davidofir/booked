﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Derived class from Doctor which inherits from an abstract User.
/// Dermatologist can implement their own methods different from other type of doctors such as a dentist.
/// </summary>
/// 
namespace BookedAppLogic
{
    public class Dermatologist:Doctor
    {
        private string _name;
        private string _email;
        private string _password;
        private DayOfWeek _dayOff;
        private TimeSpan _duration;
        private DateTime _startTime;
        private TimeSpan _workHours;
        private string _gender;

        //If dermatologist had their own FV
        public Dermatologist(string name, string email, string password, string gender, TimeSpan duration, DateTime startTime, TimeSpan workHours, DayOfWeek dayOff) :base(name,email,password,gender,duration,startTime,workHours,dayOff)
        {
            _name = name;
            _email = email;
            _password = password;
            _duration = duration;
            _startTime = startTime;
            _workHours = workHours;
            _gender = gender;
            _dayOff = dayOff;
        }

        //MEthods special to dermalotogists-none



    }   
}
