﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookedAppLogic
{
    public class Client : User
    {
        private string _name;
        private string _email;
        private string _password;
        private string _gender;

        public Client(string name, string email, string password, string gender) : base(name, email, password, gender)
        {
            _name = name;
            _email = email;
            _password = password;
            _gender = gender;
            
        }
    }
}
