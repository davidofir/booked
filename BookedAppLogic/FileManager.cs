﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;

/// <summary>
/// Class for managing files within the application. 
/// Loading information into application on startup
/// Updating information on a datalayer text file
/// Adding a log entry on a datalayer text file
/// Note:Logic for handling datalayer interactions. Datalayer should be accessible by this class.
/// </summary>
/// 

namespace BookedAppLogic
{
    class FileManager
    {
        //Public methods so that BookedAppLogic and Booked can reference;other assembly within solution
        public static void onRead()    
        {
            

        }

        public static void onUpdate()
        {
            //Method to write to files for updating purpose
            //Open file thats needed
        }

        public static void onLog()
        {
            //Method to write to files for logging purpose
        }
        
        private async void readFile()
        {
            /*
            var stream = await AppointmentsDB.OpenAsync(Windows.Storage.FileAccessMode.Read);    //File location error
            ulong size = stream.Size;

            string appointmentDataFrmTxt;  //Local string variable to hold the retrieved string from text file using readFile method

            using (var inputStream = stream.GetInputStreamAt(0))
            {
                uint numBytesLoaded = await DataReader.LoadAsync((uint)size);
                string text = DataReader.ReadString(numBytesLoaded);
            }
            /*
            //Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            //Windows.Storage.StorageFile sampleFile = await storageFolder.GetFileAsync("AppointmentsDB.txt");
            appointmentDataFrmTxt = await Windows.Storage.FileIO.ReadTextAsync("AppointmentsDB.txt");
            */
            
        }



    }
}
