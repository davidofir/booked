﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookedAppLogic
{
    public class FamilyDoctor : Doctor
    {
        private string _name;
        private string _email;
        private string _password;
        private string _gender;
        private DayOfWeek _dayOff;
        private TimeSpan _duration;
        private DateTime _startTime;
        private TimeSpan _workHours;

        //If dermatologist had their own FV
        public FamilyDoctor(string name, string email, string password, string gender, TimeSpan duration, DateTime startTime, TimeSpan workHours,DayOfWeek dayOff) : base(name, email, password, gender, duration, startTime, workHours, dayOff)
        {
            _name = name;
            _email = email;
            _password = password;
            _gender = gender;
            _duration = duration;
            _startTime = startTime;
            _workHours = workHours;
            _dayOff = dayOff;
        }
    }
}
