﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Perception;
using Windows.UI.Xaml;

/// <summary>
/// Description:
/// This Booked class will be the object representation of a service provider available on the application.
/// This class will contain the information of a service provider, including their name,license,location,availability, and services.
/// Technical:
/// This class is a derived class of User since service providers are also users of the application. Service providers have
/// different options availanble to them during interaction.
/// 
/// Doctors temporarily named due to Doctor.cs collision -> To be fixed.
/// </summary>

namespace BookedAppLogic
{
    public class Doctor : User
    {
        private string _name;
        private int _Id;
        private static int S_counter = 1000;
        private string _email;
        private string _password;
        private DayOfWeek _dayOff;
        //private Service _service;
        private List<Appointment> _availAppointments;
        private List<Appointment> _unavailAppointments;
        private DateTime _startTime;
        //Derived classes have different durations : Dermatologist:1hr
        private TimeSpan _duration;
        private TimeSpan _workHours;        //work hours for a doctor, different for different docs
        private string _gender;


        public Doctor(string name, string email, string password, string gender, TimeSpan duration, DateTime startTime, TimeSpan workHours, DayOfWeek dayoff) : base(name, email, password, gender)
        {
            _Id = (Increment());
            _name = name;
            _availAppointments = new List<Appointment>();
            _unavailAppointments = new List<Appointment>();
            _duration = duration;
            _startTime = startTime;
            _email = email;
            _password = password;
            _workHours = workHours;
            _gender = gender;
            _dayOff = dayoff;
            CreateAppointments();
        }
        public int Increment()
        {
            return S_counter++;
        }
        //public string location { get => _location; set => _location = value; }
        //public string license { get => _license; set => _license = value; }


        //Need to set schedule and service; objects.
        public DateTime StartTime { get => _startTime; set => _startTime = value; }
        public TimeSpan Duration { get => _duration; set => _duration = value; }
        public TimeSpan WorkHours { get => _workHours; set => _workHours = value; }
        public List<Appointment> AvailAppointments { get => _availAppointments; set => _availAppointments = value; }
        public List<Appointment> UnavailAppointments { get => _unavailAppointments; set => _unavailAppointments = value; }
        public int Id { get => _Id;}

        //Creating the appointments with a day off according to the day that was specified by the Doctor
        public void CreateAppointments()
        {
            DateTime nextDate = _startTime;
            for (int day=0;day<14;day++)
            {
                //if the day is our dayoff, skip the statement.
                if (nextDate.DayOfWeek==_dayOff)
                {
                    nextDate = new DateTime(nextDate.Year, nextDate.Month, nextDate.Day, _startTime.Hour, _startTime.Minute, _startTime.Second).AddDays(1);
                    continue;
                }
                else
                {
                    Appointment appointment;
                    //creating the appointments for the day, the for loop value determines the amount of appointments that we will have in our day
                    for (int app = 0; app < (WorkHours / Duration); app++)
                    {
                        appointment = new Appointment(nextDate, _duration);
                        nextDate += _duration;
                        _availAppointments.Add(appointment);
                    }
                    nextDate = new DateTime(nextDate.Year,nextDate.Month,nextDate.Day,_startTime.Hour,_startTime.Minute,_startTime.Second).AddDays(1);
                }
            }
        }
        //moves an element form a list to another and resorts the lists by their date, and keep both of them ordered by the Date property
        public (List<Appointment>,List<Appointment>) ChangeStatus(List<Appointment>list1,List<Appointment>list2,int index)
        {
            list2.Add(list1[index]);
            list1.RemoveAt(index);
            list2.OrderBy(o => o.Date).ToList();
            list1.OrderBy(o => o.Date).ToList();
            return (list1, list2);
        }
    }
}
